module.exports = {
  pluginOptions: {
    electronBuilder: {
      builderOptions: {
        copyright: 'Copyright©pandaofc',
        win: {
          icon: 'public/icon.ico',
          target: [{
            target: 'nsis',
            arch: ['x64', 'ia32']
          }]
        },
        // options placed here will be merged with default configuration and passed to electron-builder
      }
    }
  }
}