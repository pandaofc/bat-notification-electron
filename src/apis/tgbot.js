import axios from 'axios'

const baseURL = 'https://api.telegram.org/';
axios.defaults.baseURL = baseURL;
axios.defaults.headers = {
    'Content-Type': 'application/json',
}
export async function getUpdates() {
    const token = localStorage.getItem('token')
    return await axios.get(`/bot${token}/getUpdates`);
}

export async function sendMessage(text) {
    const token = localStorage.getItem('token')
    const chat_id = localStorage.getItem('chat_id')
    return await axios.post(`/bot${token}/sendMessage`, {
        chat_id,
        text
    });
}
