import { createApp } from 'vue'
import { createStore } from 'vuex'
import App from './App.vue'

const store = createStore({
    state () {
      return {
        isConnect: false,
        lastIndex: null,
        logs: []
        
      }
    },
    mutations: {
      connectToggle (state) {
        state.isConnect = !state.isConnect
      },
      addLastIndex (state) {
        state.lastIndex = state.lastIndex + 1
        localStorage.setItem("lastIndex", state.lastIndex)
      },
      initLastIndex (state, i) {
        state.lastIndex = i
        localStorage.setItem("lastIndex", state.lastIndex)
      },
      addLogs (state, msg) {
        var date = new Date();
        var dateFormat = date.toDateString() + ' ' + date.toLocaleTimeString();

        state.logs.push(`[${dateFormat}] ${msg}`)
      }
    }
  })

createApp(App).use(store).mount('#app')
