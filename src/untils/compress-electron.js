import path from "path";
const csvJson = require('csvtojson')
const { app } = require("electron");

export async function getTradeLog() {
  const appPath = getAppRoot();

  var p = path.join(appPath, "trade_log.csv");
  console.log('file: ' + p)
  return await csvJson({ noheader: true }).fromFile(p);
}

export function getFilePath() {
  const appPath = getAppRoot();
// console.log(appPath)
  var p = path.join(appPath, "trade_log.csv");
  return p;
}


export function getAppRoot() {
  if (process.platform === 'win32') {
    return path.join(app.getAppPath(), '/../../../');
  } else {
    return path.join(app.getAppPath(), '/../../../../');
  }
}